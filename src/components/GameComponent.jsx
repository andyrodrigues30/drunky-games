import React, { useEffect, useState } from "react";
import "../styles/Game.css";

import ErrorPage from "../screens/ErrorPage";
import Sixes from "../games/Sixes";
import NeverHaveIEver from "../games/NeverHaveIEver";

const games = require("../data/data.json");

export default function GameComponent(props) {
    const [game, setGame] = useState({});

    useEffect(() => {
        for (let i = 0; i < games.length; i++) {
            if (games[i].Id === props.id) {
                setGame(games[i]);
            }
        }
    }, [props.id]);

    switch (props.id) {
        case "sixes":
            return (
                <div className="GameComponent">
                    <Sixes game={game} />
                </div>
            );
        case "never-have-i-ever":
            return (
                <div className="GameComponent">
                    <NeverHaveIEver game={game} />
                </div>
            );
        case "who-is-most-likely-to":
            return (
                <div className="GameComponent">
                    <h1>{game.Title}</h1>
                    <p>...become a millionaire</p>
                </div>
            );
        default:
            return (
                <div className="GameComponent">
                    <ErrorPage errorMessage={"This game is not found. Top up your glass and choose a different game."} />
                </div>
            );
    }
}

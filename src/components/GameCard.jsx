import React from "react";
import "../styles/Home.css";

export default function GameCard(props) {
  return(
    <div className="GameCard">
      <h2>{props.title}</h2>
      <p>{props.desc}</p>
    </div>
  );
}

import React from "react";
import { Link } from "react-router-dom";
import GameCard from "./GameCard";
const games = require("../data/data.json");

export default function GamesList() {
  return (
    <div className="GamesList">
      {
        games.map(game => (
          <Link to={`/game/${game.Id}`} key={game.Id} style={{ textDecoration: "none", color: "#E6E5E7" }} >
            <GameCard title={game.Title} desc={game.Description} />
          </Link>
        ))
      }
    </div>
  );
}

import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "./App.css";

import Home from "./screens/Home";
import Game from "./screens/Game";
import ErrorPage from "./screens/ErrorPage";

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/games" exact component={Home} />
          <Route path="/game/:id" component={Game} />
          <Route component={() => <ErrorPage errorMessage={"The url doesn't seem to be working. Top up your glass, check the url or go to games."} />}/>
        </Switch>
      </div>
    </Router>
  );
}

export default App;

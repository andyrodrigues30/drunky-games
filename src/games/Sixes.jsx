import React, { useState } from "react";
import { Link } from "react-router-dom";

import "../styles/Game.css";

import DiceOne from "../assets/dice/dice1.png";
import DiceTwo from "../assets/dice/dice2.png";
import DiceThree from "../assets/dice/dice3.png";
import DiceFour from "../assets/dice/dice4.png";
import DiceFive from "../assets/dice/dice5.png";
import DiceSix from "../assets/dice/dice6.png";
import Play from "../assets/dice/Play.png";

export default function Sixes(props) {
    const game = props.game;
    const [dice, setDice] = useState(DiceOne)
    const [num, setNum] = useState(1)
    return (
        <div className="Sixes">
            <h1>{game.Title}</h1>
            
            <div className="GameContent">
                <img id="Dice" src={dice} alt="Dice" />

                <p>Drink from cup number {num}</p>

                <div id="PlayBtn" onClick={() => {
                    let rndNum = Math.floor(Math.random() * 6) + 1;
                    switch (rndNum) {
                        case 1:
                            setNum(1);
                            setDice(DiceOne);
                            break;
                        case 2:
                            setNum(2);
                            setDice(DiceTwo);
                            break;
                        case 3:
                            setNum(3);
                            setDice(DiceThree);
                            break;
                        case 4:
                            setNum(4);
                            setDice(DiceFour);
                            break;
                        case 5:
                            setNum(5);
                            setDice(DiceFive);
                            break;
                        case 6:
                            setNum(6);
                            setDice(DiceSix);
                            break;
                        default:
                            console.log(1)
                            setDice(DiceOne);
                            break;
                    }
                }}>
                    <img src={Play} alt="Roll-Button" />
                </div>

                <Link to={"/"} style={{ textDecoration: "none", color: "#E6E5E7" }} >
                    <div className="Finish">
                        <p>Finish</p>
                    </div>
                </Link>
            </div>
        </div>
    )
}

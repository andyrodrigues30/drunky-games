import React, { useState } from 'react'
import { Link } from "react-router-dom";

import "../styles/Game.css";

import Play from "../assets/dice/Play.png";

export default function NeverHaveIEver(props) {
    const game = props.game;
    const [neverHaveIEverCont, setneverHaveIEverCont] = useState("pulled a push door");

    return (
        <div className="NeverHaveIEver">
            <h1>{game.Title}</h1>

            <div className="GameContent">
                <p id="NeverHaveIEverCont">...{neverHaveIEverCont}</p>

                <div id="PlayBtn" onClick={() => {
                    const data = game.Data;
                    const dataLength = data.length;
                    let rndNum = Math.floor(Math.random() * dataLength);
                    setneverHaveIEverCont(data[rndNum]);
                }}>
                    <img src={Play} alt="Next-Button" />
                </div>
            </div>

            <Link to={"/"} style={{ textDecoration: "none", color: "#E6E5E7" }} >
                <div className="Finish">
                    <p>Finish</p>
                </div>
            </Link>
        </div>
    )
}

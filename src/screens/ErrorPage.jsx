import React from "react";
import { Link } from "react-router-dom";

import "../styles/ErrorPage.css";
import RedCup from "../assets/Red-Cup.png";

export default function ErrorPage(props) {
    return (
        <div className="ErrorPage">
            <div className="Header">
                <div className="Title">
                    <h1 id="ErrorHeading">404</h1>
                </div>
                <img src={RedCup} alt="Red-Cup" />
            </div>

            <div className="ErrorContainer">
                <h2>Oooopss...</h2>
                <p>{props.errorMessage}</p>
            </div>

            <Link to={"/"} style={{ textDecoration: "none", color: "#E6E5E7" }} >
                <div className="GoToGames">
                    <p>Go to Games</p>
                </div>
            </Link>
        </div>
    );
}

import React from "react";
import "../styles/Home.css";

import GamesList from "../components/GamesList";
import RedCup from "../assets/Red-Cup.png";

export default function Home() {
    return (
        <div className="Home">
            <div className="Header">
                <div className="Title">
                    <h1>Drunky</h1>
                    <h1>Games</h1>
                </div>

                <img src={RedCup} alt="Red-Cup" />
            </div>
            <GamesList />
        </div>
    );
}

import React from "react";
import GameComponent from "../components/GameComponent";
import "../styles/Game.css";

export default function Game({ match }) {
    return (
        <div className="Game">
            <div className="GameTitle">
                <GameComponent id={match.params.id} />
            </div>
        </div>
    );
}

![Logo](https://gitlab.com/andyrodrigues30/drunky-games/-/raw/master/designs/logo.png)

# Drunky Games

Drunky Games is a collection of fun drinking games to play with friends and family.

This web app is create in React.js Node.js Express.js.
The app is hosted on Heroku.

## View the live web app here
[https://drunky-games.herokuapp.com/](https://drunky-games.herokuapp.com/)

## Run App
In the project directory:

```
npm start
```

Then in your browser go to ```http://localhost:8080/```


## Designs
### Color Scheme
![Color Scheme](https://gitlab.com/andyrodrigues30/drunky-games/-/raw/master/designs/scheme.png)

### Screens
#### Home
![Home Screen](https://gitlab.com/andyrodrigues30/drunky-games/-/raw/master/designs/home.png)

#### Sixes
![Sixes Screen](https://gitlab.com/andyrodrigues30/drunky-games/-/raw/master/designs/sixes.png)

#### Never have I ever
![Never Have I Ever Screen](https://gitlab.com/andyrodrigues30/drunky-games/-/raw/master/designs/never_have_i_ever.png)

#### Back to back
![Back To Back Screen](https://gitlab.com/andyrodrigues30/drunky-games/-/raw/master/designs/back_to_back.png)
